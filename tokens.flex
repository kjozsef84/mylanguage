%{
/*
dot = float
dotless = int
transform = explicit tipuskonverzio
*/
#include <iostream>
#include <stdio.h>
#include<string>
using namespace std;

char temp []= "[sor: %d, oszlop: %d, hossz %d] %s \n";
char buffer[1000];
int columnNumber = 1;

%}
%option noyywrap
%option lex-compat
%%

dot { 
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
    }

dotless {
            sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
            cout << buffer;
            columnNumber += strlen(yytext);
        }


come {
            sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
            cout << buffer;
            columnNumber += strlen(yytext);
    }

go {
            sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
            cout << buffer;
            columnNumber += strlen(yytext);
    }

more {
            sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
            cout << buffer;
            columnNumber += strlen(yytext);
    }


eq {
            sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
            cout << buffer;
            columnNumber += strlen(yytext);
    }


neq {
            sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
            cout << buffer;
            columnNumber += strlen(yytext);
    }

_([A-Za-z0-9]{2,}) {
                    sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), "VALTOZONEV");
                    cout << buffer;
                    columnNumber += strlen(yytext);
                }


let {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
    }

be {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
    }

-?\+?[0-9]+ {
        sprintf (buffer, "[sor: %d, oszlop: %d, hossz %d] %s %s \n", yylineno, columnNumber, strlen(yytext), "EGESZSZAM", yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
        }

-?\+?[0-9]+"."[0-9]+ {
            sprintf (buffer, "[sor: %d, oszlop: %d, hossz %d] %s %s \n", yylineno, columnNumber, strlen(yytext), "VALOSSZAM ", yytext);
            cout << buffer;
            columnNumber += strlen(yytext);
        }

\+ {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), "OSSZEADAS");
        cout << buffer;
        columnNumber += strlen(yytext);
    }

- {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), "KIVONAS");
        cout << buffer;
        columnNumber += strlen(yytext);
    }

\* {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), "SZORZAS");
        cout << buffer;
        columnNumber += strlen(yytext);
    }

ha {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
    }

haha {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
    }

loop {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
    }

transform {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext); 
    }

\! {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), "LOGIKAI TAGADAS");
        cout << buffer;
        columnNumber += strlen(yytext);
}

and {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), "LOGIKAI ES");
        cout << buffer;
        columnNumber += strlen(yytext);
}

\{ {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
    }

\} {
        sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), yytext);
        cout << buffer;
        columnNumber += strlen(yytext);
    }

[ \t] {
        columnNumber++;
    }


\n|\n\r {
            columnNumber = 1;
            //cout << endl;
        }

. {
            sprintf (buffer, temp, yylineno, columnNumber, strlen(yytext), "ISMERETLEN SZIMBOLUM");
            cout << buffer;
            columnNumber += strlen(yytext);
    }

%%

int main() {
 
    yylex();

}
